###################################################
## This defines the m_cmif
## This provides the weights of m_cmif.
###################################################

m_cmif <- function(){
  dta <- tibble::tribble(
    ~assets,             ~weights,        ~date,
    "m_renewable_energy",    0.34, "2016-01-01",
    "m_green_bonds",         0.30, "2016-01-01",
    "m_cash",                0.05, "2016-01-01",
    "m_social_housing",      0.16, "2016-01-01",
    "m_tech_for_good",       0.08, "2016-01-01",
    "m_micro_finance",       0.07, "2016-01-01"
  )

  return(dta)
}